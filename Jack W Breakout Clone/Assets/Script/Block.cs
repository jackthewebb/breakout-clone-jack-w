using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;


public class Block : NetworkBehaviour
{
    bool dead;

    //gives points and destroys the object if hit
    public void GetHit()
    {
        if (!dead) { 
        
        FindObjectOfType<GameManager>().AddScore();
        dead = true;
        Destroy(gameObject);      
        }
    }
    //changes color to make a rainbow pattern
    public void ChangeColor(Material color)
    {
        foreach (Transform child in transform)
        {
            child.GetComponent<MeshRenderer>().material = color;
        }

    }
}
