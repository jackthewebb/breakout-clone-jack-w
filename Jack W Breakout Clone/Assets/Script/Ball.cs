using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Ball : NetworkBehaviour
{

    public GameObject lastHitPaddle;
    public Rigidbody rb;
    private float velocity = 15f;

 
    private void Start()
    {
        rb = GetComponent<Rigidbody>();

    }
    //this happens when the ball touches something
    private void OnTriggerEnter(Collider other)
    {
            if (other.CompareTag("RedZone"))
            {
                Respawn();
            }
            if (other.CompareTag("Paddle"))
            {
                Vector3 newVelocity = new Vector3(rb.velocity.x, rb.velocity.y * -1, 0);
                rb.velocity = newVelocity + other.GetComponent<Rigidbody>().velocity;
                rb.velocity = velocity * (rb.velocity.normalized);

                lastHitPaddle = other.gameObject;
            }
            if (other.CompareTag("Wall"))
            {
                Vector3 newVelocity = new Vector3(rb.velocity.x * -1, rb.velocity.y, 0);
                rb.velocity = newVelocity;
                if (other.GetComponentInParent<Block>() != null)
                {
                    other.GetComponentInParent<Block>().GetHit();
                }
            }
            if (other.CompareTag("Roof"))
            {
                if (other.GetComponentInParent<Block>() != null)
                {
                    other.GetComponentInParent<Block>().GetHit();
                }
                Vector3 newVelocity = new Vector3(rb.velocity.x, rb.velocity.y * -1, 0);


                rb.velocity = newVelocity;
            }
            if (other.CompareTag("Brick"))
            {
                other.GetComponentInParent<Block>().GetHit();
            }
        
    }
    //the ball goes back to the player who last hit the ball
    //if they already have a ball, it goes to the other player instead
    void Respawn()
    {
        if(lastHitPaddle.GetComponent<Paddle>().heldBall == null)
        {
        lastHitPaddle.GetComponent<Paddle>().heldBall = gameObject;
        }else
        {
            foreach(Paddle p in FindObjectsOfType<Paddle>())
            {
                if(p.heldBall == null)
                {

                    p.heldBall = gameObject;
                }
            }
        }
    }
    //launches the ball
    public void ServeBall(Paddle paddle)
    {
        paddle.heldBall = null;
        float angle = Random.Range(45f, 135f);
        float velX = velocity * Mathf.Cos(angle * Mathf.Deg2Rad);
        float velY = velocity * Mathf.Sin(angle * Mathf.Deg2Rad);
        rb.velocity = new Vector3(velX, velY, 0);
    }
}
