using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;


public class GameManager : NetworkBehaviour
{
    public Text scoreText;
    public Text serveText;
    public Transform spawnLocation;
    public GameObject brick;
    public List<Material> brickColors = new List<Material>();
    int blockDestroyedCount;
    int score;

    void Start()
    {
        SpawnBricks();
    }
    void SpawnBricks()
    {
        for (int i = 0; i < 5; i++)
        {
            Vector3 spawnposV = new Vector3(spawnLocation.position.x, spawnLocation.position.y + 2f * i, 0);
            GameObject newBrickV = Instantiate(brick, spawnposV, Quaternion.identity);
            newBrickV.GetComponent<Block>().ChangeColor(brickColors[i]);

            for (int h = 0; h < 9; h++)
             {
                 Vector3 spawnposH = new Vector3(spawnLocation.position.x + 2f * (h+1), spawnLocation.position.y + 2f * i, 0);
                 GameObject newBrickH = Instantiate(brick, spawnposH, Quaternion.identity);
                 newBrickH.GetComponent<Block>().ChangeColor(brickColors[i]);
            }
        }
    }
    public void AddScore()
    {
        score += 100;
        scoreText.text = "Score: " + score.ToString();
        blockDestroyedCount += 1;
        if(blockDestroyedCount == 50)
        {
            SpawnBricks();
            blockDestroyedCount = 0;
        }
    }
    public void ShowServeText()
    {
        serveText.gameObject.GetComponent<CanvasGroup>().alpha = 1;

    }
    public void HideServeText()
    {
        serveText.gameObject.GetComponent<CanvasGroup>().alpha = 0;

    }
}
