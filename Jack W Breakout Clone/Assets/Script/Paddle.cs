using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Paddle : NetworkBehaviour
{
    private float boundaryL = -10.13f;
    private float boundaryR = 6.56f;
    private int speed = 80;
    [SyncVar] public GameObject heldBall;
    public GameObject ball;


    private void Start()
    {

        //changes color to green so you can tell which paddle is yours
        if (isLocalPlayer)
        {
            gameObject.GetComponent<MeshRenderer>().material.color = Color.green;
        }
     
    }
    private void Update()
    {
        HoldBall();

        //check for user input for movement and serving
        if (isLocalPlayer)
        {
            PaddleMovement();
            
            if (Input.GetKeyDown("space") && heldBall != null)
            {
                FindObjectOfType<GameManager>().HideServeText();
                LaunchBall();
            }
        }
    }
    //keeps the ball locked in place above the paddle until it's served
    void HoldBall()
    {
        if (heldBall)
        {
            heldBall.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + .7f, 0);
            heldBall.GetComponent<Ball>().rb.velocity = new Vector3(0, 0, 0);
        }
    }

    //tells the server to launch the ball
    [Command]
    void LaunchBall()
    {
        heldBall.GetComponent<Ball>().ServeBall(this);
        heldBall = null;
    }

    //moves the paddle horizontally between the boundaries
    void PaddleMovement()
    {
        Vector3 Move = new Vector3(Input.GetAxis("Horizontal"), 0, 0);
        if (Input.GetAxis("Horizontal") < 0 && transform.position.x > boundaryL)
        {
            GetComponent<Rigidbody>().MovePosition(transform.position + Move * Time.deltaTime * speed);
        }
        if (Input.GetAxis("Horizontal") > 0 && transform.position.x < boundaryR)
        {
            GetComponent<Rigidbody>().MovePosition(transform.position + Move * Time.deltaTime * speed);
        }
    }
}
