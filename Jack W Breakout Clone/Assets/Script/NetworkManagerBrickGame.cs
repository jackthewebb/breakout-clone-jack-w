using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

[AddComponentMenu("")]
public class NetworkManagerBrickGame : NetworkManager {

    public Transform player1spawn;
    public Transform player2spawn;
    public GameObject ball;

    public override void OnServerAddPlayer(NetworkConnection conn)
    {

        Physics.IgnoreLayerCollision(6, 6);
        Transform start = null;
        if(numPlayers == 0) {
            start = player1spawn;
        }else if(numPlayers == 1)
        {
            start = player2spawn;
        }
       
        GameObject newPlayer = Instantiate(playerPrefab, start.position, start.rotation);
        NetworkServer.AddPlayerForConnection(conn, newPlayer);


       
        GameObject newBall = Instantiate(ball);
        NetworkServer.Spawn(newBall);
        newPlayer.GetComponent<Paddle>().heldBall = newBall;
        newBall.GetComponent<Ball>().lastHitPaddle = newPlayer;
        
    }


    public override void OnServerDisconnect(NetworkConnection conn)
    {
        if(ball != null)
        {
            NetworkServer.Destroy(ball);
        }
        base.OnServerDisconnect(conn);
    }











}
 





